# ArgoCD

## Connect to the AKS  & Do all the commands later in the AKS
~~~
az login
~~~
~~~
az account set --subscription <subscription ID>
~~~
~~~
az aks get-credentials --resource-group <RESOURCE_GROUP_NAME> --name <AKS_CLUSTER_NAME>
~~~

## creates a Kubernetes namespace called "argocd"
~~~
kubectl create namespace argocd
~~~

## Deploy ArgoCD in the argocd namespace:
~~~
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
~~~

## Get a password tor ArgoCD
~~~
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath='{.data.password}' | ForEach-Object { [System.Text.Encoding]::UTF8.GetString([System.Convert]::FromBase64String($_)) }
~~~

## Change the argocd-server service type to LoadBalancer
~~~
kubectl patch svc argocd-server -n argocd -p '{"spec": {"type": "LoadBalancer"}}'\
~~~

## list all the services in the "argocd" namespace
~~~
kubectl get svc -n argocd
~~~

## Connect to ArgoCD
~~~
kubectl port-forward -n argocd service/argocd-server 9090:80
http://localhost:80
~~~

## Manual removal of ArgoCD from the argocd namespace (cleanup):
~~~
kubectl delete -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml

kubectl delete namespace argocd
~~~
